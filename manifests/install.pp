# == Class: supersudo
#
# Full description of class supersudo here.
#
# === Parameters
#
# Document parameters here.
#
# [*sample_parameter*]
#   Explanation of what this parameter affects and what it defaults to.
#   e.g. "Specify one or more upstream ntp servers as an array."
#
# === Variables
#
# Here you should define a list of variables that this module would require.
#
# [*sample_variable*]
#   Explanation of how this variable affects the funtion of this class and if
#   it has a default. e.g. "The parameter enc_ntp_servers must be set by the
#   External Node Classifier as a comma separated list of hostnames." (Note,
#   global variables should be avoided in favor of class parameters as
#   of Puppet 2.6.)
#
# === Examples
#
#  class { supersudo:
#    servers => [ 'pool.ntp.org', 'ntp.local.company.com' ],
#  }
#
# === Authors
#
# Author Name <author@domain.com>
#
# === Copyright
#
# Copyright 2014 Your name here, unless otherwise noted.
#
class supersudo::install {

  group { $supersudo::group_name:
    ensure  => present,
    require => [Class['sudo'], Class['augeas']]
  }

  augeas { 'sudoers_supersudo':
    context => $supersudo::sudoers_file,
    changes => [
      "set spec[user = '%${supersudo::group_name}']/user %${supersudo::group_name}",
      "set spec[user = '%${supersudo::group_name}']/host_group/host ALL",
      "set spec[user = '%${supersudo::group_name}']/host_group/command ALL",
      "set spec[user = '%${supersudo::group_name}']/host_group/command/runas_user ALL",
      "set spec[user = '%${supersudo::group_name}']/host_group/command/tag NOPASSWD"
    ],
    require => Group[$supersudo::group_name]
  }

  User<| tag == 'supersudo' |> {
    groups +> 'supersudo'
  }

}
