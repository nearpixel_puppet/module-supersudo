# Description

Adds a super sudo group and sudoers entry.

# License

Released under the Apache License, Version 2.0.
